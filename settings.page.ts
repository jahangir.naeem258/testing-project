import {Component, OnInit} from '@angular/core';
import {DatePipe, Location} from '@angular/common';
import {DarkModeService} from 'src/app/services/dark-mode.service';
import {AlertController, ModalController, Platform, MenuController} from '@ionic/angular';
import {ApiService} from "src/app/services/api.service";
import {Storage} from "@ionic/storage";
import {CommonService} from "src/app/services/common.service";
import {Device} from '@ionic-native/device/ngx';
import {EventProviderService} from "src/app/services/event-provider.service";
import {TranslateService} from "@ngx-translate/core";
import {InAppBrowser, InAppBrowserOptions} from "@ionic-native/in-app-browser/ngx";
import {EmailComponent} from "./email/email.component";
import {StatusBar} from "@ionic-native/status-bar/ngx";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { LoaderService } from 'src/app/services/loader.service';
import {environment} from "src/environments/environment";

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
    providers: [DatePipe, EmailComponent]

})
export class SettingsPage implements OnInit {

    darkmode: boolean = false;
    topics: any;
    ch_id: any;
    device_id: any;
    type: boolean = false;
    is_mobile: any = false;
    prayerwall_notofication_enable: boolean = false;
    lang: any = '';

    options : InAppBrowserOptions = {
        closebuttoncaption : 'Close',
        closebuttoncolor: '#FFFFFF',
        disallowoverscroll: 'no',
        hidenavigationbuttons: 'no',
        toolbar: 'yes',
        toolbarposition: 'top',
        location: 'yes'
    };
    environment: any = environment;
    settings_translation: any = "";
    app_download_url: any = "";
    app_download_url_iphone: any = '';
    app_download_url_android: any = '';
    ttb_landing_page_available: boolean = false;
    text_direction = this.environment.text_direction;
    icon_name_header = this.text_direction === 'rtl' ? 'arrow-forward' : 'arrow-back';
    icon_name_list = this.text_direction === 'rtl' ? 'arrow-back' : 'arrow-forward';

    constructor(
        private location: Location,
        private darkModeService: DarkModeService,
        private statusBar: StatusBar,
        private apiService: ApiService,
        private storage: Storage,
        private common: CommonService,
        private device: Device,
        private platform: Platform,
        public modalController: ModalController,
        private event_provider: EventProviderService,
        private translate: TranslateService,
        private iab: InAppBrowser,
        public alertController: AlertController,
        private menu: MenuController,
        private socialSharing: SocialSharing,
        private loader: LoaderService
    ) {
        this.loader.presentLoading().then( () => {});
        // get translated alert message
        let interval = setInterval(() => {
            this.translate.get('SETTINGS').subscribe( (res: any) => {
                this.settings_translation = res;
                this.loader.stopLoading();
                clearInterval(interval);
            });
        }, 1500)
    }

    ionViewDidEnter(){
        this.event_provider.updateurl('settings'); // to change sidebar icon color
    }

    ngOnInit() {
        this.darkmode = this.darkModeService.getDark_mode();
        if (localStorage.getItem('is_prayerwall_notofication_enable') == 'true') {
            this.prayerwall_notofication_enable = true;
        } else {
            this.prayerwall_notofication_enable = false;
        }

        // getting theme from local storage
        this.storage.get("theme").then(res => {
            if (res) {
                this.ch_id = res.ch_id;

                if (environment.is_ttb_language_app){
                    this.apiService.getTTBlangData().subscribe( (data: any) => {
                      if (data && data.status){
                          this.app_download_url = data.data.landing_page_url ? data.data.landing_page_url : 'URL N/A';
                          this.ttb_landing_page_available = true;
                      } else {
                          if (res.app_download_url){
                              let url_data = JSON.parse(res.app_download_url).find( x => x.lang_code === environment.app_language);
                              if (url_data){
                                  this.app_download_url = url_data;
                                  this.app_download_url_iphone = url_data.ios_app_url;
                                  this.app_download_url_android = url_data.android_app_url;
                                  this.ttb_landing_page_available = false;
                              }
                          }
                      }
                    }, error => {

                    })
                }

                if (this.ch_id === '1000'){
                    let lang = JSON.parse(localStorage.getItem('ism_app_language'));
                    this.lang = lang.name;
                }

                this.platform.ready().then(() => {
                    if(localStorage.getItem('device_id')){
                        this.device_id = localStorage.getItem('device_id');
                    }

                    this.getPrayerWallToggleStatus();
                    });
                }
        });
        this.translate.addLangs(['en', 'es', 'ar', 'fr']);
    }

    // setting mobile mode on toggle change
    setMode() {
        if (this.darkmode === false) {
            this.statusBar.backgroundColorByHexString("#292929");
            // this.events.publish('change-darkmode');
            this.event_provider.changedarkmode();               // calling event
            this.event_provider.changedarkmodeagain(true);               // calling event
            this.darkmode = true;
            this.darkModeService.setDark_mode(this.darkmode);
        } else {
            this.darkmode = false;
            if(!this.platform.is('android')){
                this.statusBar.backgroundColorByHexString("#FFFFFF");
            }
            // this.events.publish('change-darkmode');
            this.event_provider.changedarkmode();               // calling event
            this.event_provider.changedarkmodeagain(false);               // calling event
            this.darkModeService.setDark_mode(this.darkmode);
        }
    }

    // subscription to prayer wall notifications
    setPrayerwallNotifications() {
        this.loader.presentLoading().then(() => {});
        this.prayerwall_notofication_enable = !this.prayerwall_notofication_enable;
        if (this.prayerwall_notofication_enable) {
            localStorage.setItem('is_prayerwall_notofication_enable', 'true');
        } else {
            localStorage.setItem('is_prayerwall_notofication_enable', 'false');
        }
        const body = JSON.stringify({
            church_id: this.ch_id,
            device_id: this.device_id,
            type: this.prayerwall_notofication_enable
        });
        this.apiService.prayerwall_notification_subscribe(body).subscribe((res: any) => {
            if (res && res.status) {
            }
            this.loader.stopLoading();
        }, (err) => {
            this.loader.stopLoading();
        })
    }

    // get Status of prayer wall notification subscription of current device
    getPrayerWallToggleStatus() {
        this.loader.presentLoading().then(() => {});
        const body = JSON.stringify({
            church_id: this.ch_id,
            device_id: this.device_id,
        });
        this.apiService.getPrayerWallToggleStatus(body).subscribe((res: any) => {
            if (res && res.status) {
                this.prayerwall_notofication_enable = res.toggle_value;
            }
            this.loader.stopLoading();

        }, (err) => {
            this.loader.stopLoading();
        })
    }

    // inside app on page with done button
    openabouturlinapp(){
        // about url
           let link = 'https://ttb.org/about/contact-us';
        if(this.platform.is('cordova')) {
            this.iab.create(link, '_system', this.options);   // target: '_blank' for in-app browser, '_system' for external browser
        } else {
            this.iab.create(link, '_self', this.options);
        }
    }

    // navigating to back page
    gotobackPage() {
            this.menu.toggle();
    }

    shareApp(){
        let share_text = this.settings_translation.share_the_app_text;
            let url_to_share = share_text;
            if (this.ttb_landing_page_available){
                url_to_share = url_to_share + this.app_download_url;
            } else {
                url_to_share = url_to_share + (this.app_download_url ? ( this.app_download_url_iphone + ' (Apple Store) ' + this.settings_translation.share_the_app_text_2 + this.app_download_url_android + ' (Google Play Store) ' ): 'URL  N/A');
            }
            share_text = url_to_share;
        this.socialSharing.share(share_text).then(data =>{

        }, error => {

        })
    }

}
